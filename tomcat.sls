tomcat:
    '8.5.20':
        full_name: 'Apache Tomcat 8.5 Tomcat8 (remove only)
        installer: http://mirror.cogentco.com/pub/apache/tomcat/tomcat-8/v8.5.20/bin/apache-tomcat-8.5.20
        locale: en_US
        reboot: False
        install_flags: '/S /D=C:\Tomcat'
        uninstaller: C:\Tomcat\Uninstall.exe
        uninstall_flags: '-ServiceName="Tomcat8"'
